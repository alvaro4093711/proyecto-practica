import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'

//libraries
import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'

//components
import Users from './router/Users.route.jsx'
import Posts from './router/Posts.route.jsx'
import Movies from './router/Movies.route.jsx'
import AddUser from './router/AddUser.route.jsx'
import AddPost from './router/AddPost.route.jsx'
import AddMovie from './router/AddMovie.route.jsx'
import Error404 from './router/Error404.route.jsx'
const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
  },
  {
    path: '/users',
    element: <Users />,
  },
  {
    path: 'add-user',
    element: <AddUser />,
  },
  {
    path: 'posts',
    element: <Posts />,
  },
  {
    path: 'add-post',
    element: <AddPost />,
  },
  {
    path: '/movies',
    element: <Movies />,
  },
  {
    path: 'add-movie',
    element: <AddMovie />,
  },
  {
    path: '*',
    element: <Error404 />,
  },
])
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <QueryClientProvider client={new QueryClient()}>
      <RouterProvider router={router}>
        <App />
        <ReactQueryDevtools />
      </RouterProvider>
    </QueryClientProvider>
  </React.StrictMode>
)

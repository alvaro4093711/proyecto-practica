import { useForm } from 'react-hook-form'
import { useMutation } from '@tanstack/react-query'
export default function AddMovie() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()
  const onSubmit = (data) => {
    console.log(data)
  }
  const onErrors = (errors) => {
    console.log(errors)
  }
  return (
    <div>
      <form
        action=''
        className='flex flex-col gap-4'
        onSubmit={handleSubmit(onSubmit, onErrors)}
      >
        <label htmlFor='name'>Name: </label>
        <input
          type='text'
          name='name'
          {...register('name', { required: 'Campo obligatorio' })}
        />
        {errors.name && <p className='text-red-800'>{errors.name.message}</p>}
        <label htmlFor='age'>Age: </label>
        <input
          type='text'
          name='age'
          {...register('age', { required: true })}
        ></input>
        <button type='submit'>submit</button>
      </form>
    </div>
  )
}

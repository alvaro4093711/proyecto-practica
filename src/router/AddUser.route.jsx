import { useMutation } from '@tanstack/react-query'
import { useForm } from 'react-hook-form'

import api from '../api/CustomApi'
export default function AddUser() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()
  const mutation = useMutation({
    mutationKey: 'addUser',
    mutationFn: api.addUser,
  })

  const onSubmit = (data) => {
    mutation.mutate(data)
  }
  return (
    <div className=''>
      <h1 className='mb-10'>Add User</h1>
      <form
        action=''
        onSubmit={handleSubmit(onSubmit)}
        className='flex flex-col gap-1 font-semibold'
      >
        <label htmlFor='nombre'>Nombre:</label>
        <input
          type='text'
          id='nombre'
          name='nombre'
          {...register('nombre', {
            required: 'campo obligatorio',
            minLength: { value: 3, message: 'minimo 3 caracteres' },
          })}
        />
        {errors.nombre && (
          <p className='text-red-800'>{errors.nombre.message}</p>
        )}

        <label htmlFor='edad'>Edad:</label>
        <input
          type='number'
          id='edad'
          name='edad'
          {...register('edad', { required: 'campo obligatorio' })}
        />
        {errors.edad && <p className='text-red-800'>{errors.edad.message}</p>}

        <label htmlFor='email'>Email:</label>
        <input
          type='email'
          id='email'
          name='email'
          {...register('email', {
            required: 'campo obligatorio',
            pattern: {
              value: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g,
              message: 'email invalido',
            },
          })}
        />
        {errors.email && <p className='text-red-800'>{errors.email.message}</p>}

        <label htmlFor='ciudad'>Ciudad:</label>
        <input
          type='text'
          id='ciudad'
          name='ciudad'
          {...register('ciudad', { required: 'campo obligatorio' })}
        />
        {errors.ciudad && (
          <p className='text-red-800'>{errors.ciudad.message}</p>
        )}

        <label htmlFor='intereses'>Intereses:</label>
        <input
          type='text'
          id='intereses'
          name='intereses'
          {...register('intereses', { required: 'campo obligatorio' })}
        />
        {errors.intereses && (
          <p className='text-red-800'>{errors.intereses.message}</p>
        )}
        <button className='mt-3'>Submit</button>
      </form>
    </div>
  )
}

import { useQuery } from '@tanstack/react-query'
import api from '../api/CustomApi'
export default function Users() {
  const query = useQuery({
    queryKey: ['users'],
    queryFn: api.getUsers,
  })

  return (
    <div className=''>
      <h1>Users</h1>
      {query.isLoading && <p>Loading...</p>}
      {query.isError && <p>Error: {query.error.message}</p>}
      {query.isSuccess && (
        <ul className='p-10 grid grid-cols-2 text-left gap-5'>
          {query.data.map((user) => (
            <li key={user.id} className='p-2 flex gap-x-2'>
              <span>
                <b>Nombre:</b> {user.nombre}
              </span>
              <span>
                <b>Email: </b>
                {user.email}
              </span>
            </li>
          ))}
        </ul>
      )}
    </div>
  )
}

import axios from 'axios'
class CustomApi {
  constructor(API_URL) {
    this.API_URL = API_URL
    this.axiosInstance = axios.create({
      baseURL: API_URL,
    })
  }
  getUsers = async () => {
    const response = await this.axiosInstance.get('/users')
    return response.data
  }
  getPosts = async () => {
    const response = await this.axiosInstance.get('/posts')
    return response.data
  }
  getMovies = async () => {
    const response = await this.axiosInstance.get('/movies')
    return response.data
  }
  addUser = async (user) => {
    const { nombre, edad, email, ciudad, intereses } = user
    if (!nombre || !edad || !email || !ciudad || !intereses) {
      throw new Error('All fields are required')
    }
    const response = await this.axiosInstance.post('/users', user)
    return response.data
  }
  addPost = async (post) => {
    const response = await this.axiosInstance.post('/posts', post)
    return response.data
  }
  addMovie = async (movie) => {
    const response = await this.axiosInstance.post('/movies', movie)
    return response.data
  }
}
const api = new CustomApi('http://localhost:3000')
export default api

import { Link } from 'react-router-dom'
import './App.css'

function App() {
  return (
    <div className='grid grid-cols-3 items-center justify-center gap-4'>
      <Link
        className='p-2 rounded-md border-2 border-indigo-700 text-xl'
        to='/users'
      >
        Users
      </Link>
      <Link
        className='p-2 rounded-md border-2 border-indigo-700 text-xl'
        to='/posts'
      >
        Posts
      </Link>
      <Link
        className='p-2 rounded-md border-2 border-indigo-700 text-xl'
        to='/movies'
      >
        Movies
      </Link>
      <Link
        className='p-2 rounded-md border-2 border-indigo-700 text-xl'
        to='/add-user'
      >
        Add User
      </Link>
      <Link
        className='p-2 rounded-md border-2 border-indigo-700 text-xl'
        to='/add-post'
      >
        Add Post
      </Link>
      <Link
        className='p-2 rounded-md border-2 border-indigo-700 text-xl'
        to='/add-movie'
      >
        Add Movie
      </Link>
    </div>
  )
}

export default App
